# pdf web scrapping

Web scraping script that will read PDF's from [here](https://gdchillers.com/our-products/) and compare them to local PDF's and sure the ones on the website are up to date.

### Basic breakdown of the script

```
Help on class pdf_comparing in module __main__:

class pdf_comparing(builtins.object)
 |  This class will grab PDF's from the G&D Chillers website
 |  then convert those pdfs into txt files then take the local
 |  pdfs we have and convert them into txt files. From there it
 |  will compare the two text files and report if anything is different.
 |  This is to check and make sure that the pdfs online are
 |  the most up to date ones without having to manually go through
 |  all of the pdfs.
 |  
 |  Data descriptors defined here:
 |  
 |  __dict__
 |      dictionary for instance variables (if defined)
 |  
 |  __weakref__
 |      list of weak references to the object (if defined)
 |  
 |  collection
 |      This will grab all the pdfs from the website
 |  
 |  compare
 |      This compares the two text files generated from
 |      the open_pdf function and tell us if there are any differences
 |      and if so, what they are.
 |  
 |  online_compare
 |      This will take the PDF's and compare them online 
 |      which can compare the photos and whatnot better than
 |      I can easily do with Python.
 |  
 |  open_pdf
 |      This will turn all the pdfs (local and from the website)
 |      into text files.
```